<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],
    'etsy' => [
        'consumer_key' => 'ojn1ifnclpzgpbqz53z22ukp',
        'consumer_secret' => 'fsjgy3d22g',
        'token_secret' => '5fcac52e01889fd4c06a8cacb6ea66',
        'token' => '48e0710fb3',
        'access_token' => '3d7f708b1711124959949794213269',
        'access_token_secret' => '7104764bfa',
    ]

];
