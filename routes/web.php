<?php

use App\Order;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index');

Route::group(['middleware' => 'auth'], function () {
        // Uses Auth Middleware
        Route::get('orders', function(){
            return view('orders.index');
        });

        Route::get('/dashboard', 'DashboardController@index');

        // ajax
        Route::get('api/db_orders/{min_date}/{max_date}', 'OrderController@byDate');
        // Route::get('api/orders/status/{order_id}', 'OrderController@getStatus');
        // Route::get('api/orders/notes/{order_id}', 'OrderController@getNote');
        Route::get('api/orders/{order_id}', 'OrderController@getOrder');
        Route::post('api/orders', 'OrderController@store');
        Route::post('api/orders/update', 'OrderController@update');
        Route::post('api/orders/delete', 'OrderController@delete');
        Route::get('api/orders/{min_date}/{max_date}/offset/{offset}', function($min_date, $max_date, $offset){
            $results = app('etsy')->findAllShopReceipts(array(
                'params' => array(
                    'shop_id' => 'pumpkinpaperco'
                ),
                'data' => array(
                        'min_created' => $min_date,
                        'max_created' => $max_date,
                        'limit' => 25,
                        'offset' => (int)$offset
                ),
                'associations' => array(
                    'Transactions' => array('limit' => 100)
                )));

            //$db_orders = Order::where('creation_tsz', '>=', $min_date)->where('creation_tsz', '<=', $max_date)->get();
            return [$results];
        });
        //Route::get('/admin', 'AdminController@index');

});


Route::group(['middleware' => 'admin'], function () {
        Route::get('/admin', 'AdminController@index');

        // ajax
        Route::get('api/users', 'AdminController@getUsers');
        Route::post('api/users', 'AdminController@saveUser');
});
