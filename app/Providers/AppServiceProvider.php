<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // any time this view is loaded, I will bind this variable to it
        // I could use this to bind access to the etsy api for every page
        // view()->composer('partials.nav', function($view){
        //     $view->with('etsy_api', \App\EtsyApi());
        // });
        Schema::defaultStringLength(191);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {

        // $this->app->singleton('\EtsyApi', function(){
        //     return \EtsyApi(config('services.etsy.secret'));
        // });

        // also if you need some other app('whatever') thing, you can pass $ app into the function above and use it to create an instance of it
    }
}
