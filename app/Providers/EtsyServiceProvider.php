<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class EtsyServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('etsy', function ($app) {
            $auth = config('services.etsy');
            $client = new \Etsy\EtsyClient($auth['consumer_key'], $auth['consumer_secret']);
            $client->authorize($auth['access_token'], $auth['access_token_secret']);
            return new \Etsy\EtsyApi($client);
        });
    }
}
