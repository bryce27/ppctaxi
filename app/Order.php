<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = [
        'etsy_order_id', 'status', 'creation_tsz'
    ];
}
