<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class AdminController extends Controller
{
    public function index(){
        return view('admin.index');
    }

    // ajax
    public function getUsers(){
        return User::all()->toArray();
    }
    public function saveUser(Request $request){
        $user = User::firstOrNew(['email' => $request->input('email')]);
        $user->name = $request->input('name');
        $user->password = bcrypt('pumpkins');
        $user->role = 'user';
        $user->save();

        return [$user];
    }
}
