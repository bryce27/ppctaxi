<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Order;

class OrderController extends Controller
{
    public $etsyApi;

    public function __construct(){
        $this->middleware('auth');
        $this->etsyApi = app('etsy');
    }

    public function index(){
        return view('orders.index');
    }

    // ajax
    public function show(){

        // update shop to name to refer to config file config('etsy')

        $date = Input::get('date');
        // date -> A whole number value representing UNIX epoch time, or any string accepted by PHP's strtotime() function.

        $results = $this->etsyApi->findAllShopReceipts(array(
            'params' => array(
                'shop_id' => 'pumpkinpaperco',
                'min_created' => $date,
            ),
            'associations' => array(
                'Transactions/Buyer'
            )));

        return $results;
    }

    public function delete(Request $request){
        return 'not implemented';
    }

    public function store(Request $request){

        $order = null;

        $order = Order::firstOrNew(['etsy_order_id' => $request->input('etsy_order_id')]);
        $order->status = $request->input('status');
        $order->creation_tsz = $request->input('creation_tsz');
        if ($order->note == null){
            $order->note = '';
        }

        //if ($request->input('status') != ''){
            $order->save();
            return response()->json($order->etsy_order_id);
        //}

        //else {
            //$order->delete();
            //return response()->json('blank status; didn\'t save');
        //}
    }

    public function update(Request $request){

        $order = null;

        $order = Order::firstOrNew(['etsy_order_id' => $request->input('etsy_order_id')]);
        $order->note = $request->input('note');
        $order->status = 'none';
        $order->creation_tsz = $request->input('creation_tsz');

        //if ($request->input('status') != ''){
            $order->save();
            return response()->json($order->etsy_order_id);
        //}

        //else {
            //$order->delete();
            //return response()->json('blank status; didn\'t save');
        //}
    }

    // public function getStatus($order_id){
    //     $order = Order::where('etsy_order_id', $order_id)->first();
    //     if ($order){
    //         return response()->json($order->status);
    //     }
    //     else return response()->json('no results');
    // }

    public function getOrder($order_id){
        $order = Order::where('etsy_order_id', $order_id)->first();
        if ($order){
            return response()->json($order);
        }
        else return response()->json('no note');
    }

    public function byDate($min_date, $max_date){

        $db_orders = Order::where('creation_tsz', '>=', $min_date)->where('creation_tsz', '<=', $max_date)->get()->toArray();

        return [$db_orders];
    }
}
