<?php

namespace App\Listeners;

use App\Events\AllOrdersPackaged;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class NotifySubscribers // implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  AllOrdersPackaged  $event
     * @return void
     */
    public function handle(AllOrdersPackaged $event)
    {
        // var_dump($event->batchOfOrders['date'] . ' orders have been packaged.')
        //$event->batchOfOrders
        // then send an email to subscribers about the batch of orders that were packaged
    }
}
