@extends('layouts.app')

@section('styles')
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.min.css" rel="stylesheet">
@endsection

@section('content')
<div class="container">

    <!-- - - - -  - - - - - - PROGRESS BAR - - - - - - - - - - -->
        <div class="col-sm-12" style="text-align: center">
            <h4 style="color: #3097D1; text-align: center"><b>Your Progress<span style="color: #2AB27B"></span></b></h4>
            <div class="progress">
              <div class="progress-bar progress-bar-warning" v-bind:style="{ width: (number_cut / order_count)*100 +'%'}">
              </div>
              <div class="progress-bar progress-bar-primary" v-bind:style="{ width: (number_packaged / order_count)*100 +'%'}">
              </div>
              <div class="progress-bar progress-bar-success" v-bind:style="{ width: (number_shipped / order_count)*100 +'%'}">
              </div>
            </div>
            <hr />
        </div>
    <!-- - - - -  - - - - - - END PROGRESS BAR - - - - - - - - - - -->

    <!-- - - - -  - - - - - - LEFT FILTER PANEL - - - - - - - - - - -->
    <div class="col-sm-4"> <!-- controls col sm 3 -->

        <div class="panel panel-default">

            <div class="panel-heading" style="text-align: center">
                <h2 class="panel-title"><b>Filter Orders</b></h2>
            </div>

            <div class="panel-body">

                <div class="row" style="margin-bottom: 0px"> <!-- date picker -->
                    <div class="col-sm-12" style="text-align: center">
                            <h5 style="color: #3097D1" style="margin-top: 0px"><b>Date</b></h5>
                            <input class="datepicker" type="text" v-model="startDate" :value="cancelDate" style="text-align: center;">
                            <input type="hidden" :value="startDate" id="hiddendate">
                            <button class="btn btn-primary btn-sm" @click="getOrdersAxios" style="margin-left: 5px">Go</button>
                            <hr />
                    </div>

                </div> <!-- end date picker -->

                <div class="row"> <!-- filter section -->
                    <div class="col-sm-12">
                        <ul class="list-group">
                          <li class="list-group-item status-button" @click="current_status = 'ready_to_cut'">
                            <span class="badge" v-if="ready_to_cut" v-cloak>@{{ready_to_cut}}</span>
                            <b>1. Ready to Print/Cut</b>
                          </li>
                          <li class="list-group-item status-button" @click="current_status = 'ready_to_package'">
                            <span class="badge" v-if="ready_to_package" v-cloak>@{{ready_to_package}}</span>
                            <b>2. Ready to Package</b>
                          </li>
                          <li class="list-group-item status-button" @click="current_status = 'ready_to_ship'">
                            <span class="badge" v-if="ready_to_ship" v-cloak>@{{ready_to_ship}}</span>
                            <b>3. Ready to Ship</b>
                          </li>
                          <li class="list-group-item status-button" @click="current_status = 'shipped'">
                            <span class="badge" v-if="shipped" v-cloak>@{{shipped}}</span>
                            <b>4. Shipped</b>
                          </li>
                          <li class="list-group-item status-button active" @click="current_status = 'all'">
                            <span class="badge" v-if="list.length" v-cloak>@{{order_count}}</span>
                            <b>All Orders</b>
                          </li>
                        </ul>
                    </div>
                </div> <!-- end filter section -->
            </div> <!-- panel body end -->
        </div> <!-- panel end -->
        {{-- <div class="alert alert-info" role="alert">
          <a href="#" class="alert-link">Quote of the day</a>
        </div> --}}
    </div> <!-- end controls col sm 3 -->

    <!-- - - - -  - - - - - - END LEFT FILTER PANEL - - - - - - - - - - -->


    <!-- - - - -  - - - - - - LIST OF ORDERS - - - - - - - - - - -->
    <div class="col-sm-6">


            <div class="panel panel-default" v-cloak> <!-- turns green when you click complete...or gray -->
                <div class="panel-heading" style="text-align: center;">

                  <h2 class="panel-title" style="display: inline-block;"><b>@{{list.length}} / @{{order_count}} Orders</b></h2>
                  <button class="btn btn-primary btn-xs pull-right" v-if="current_status == 'ready_to_ship'" @click="shipAll()">Ship All</button>
                  <div class="clearfix"></div>
                </div>

                <div class="progress" v-if="!all_loaded" style="margin-top: 20px; margin-left: 10px; margin-right: 10px;">
                  <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
                  </div>
                </div>
                <h4 v-if="!all_loaded && !list.length" style="margin: auto; text-align: center; margin-top: 15px; margin-bottom: 30px">Talking to Etsy...</h4>
                <h4 v-if="list.length && !all_loaded" style="margin: auto; text-align: center; margin-top: 15px; margin-bottom: 30px">Loading Orders...</h4>
                <h4 v-if="loading_more" style="margin: auto; text-align: center; margin-top: 15px; margin-bottom: 30px">Loading More...</h4>

                <div class="panel-body">
                    <div v-for="(order, index) in activeList" v-cloak v-if="all_loaded && !loading_more">
                            <div class="row" style="padding-left: 10px; padding-right: 10px">
                            <div class="col-xs-6">
                                <h4 style="color: #3097D1"><b>@{{order.name}}<b/></h4>
                                <div class="row no-gutters">
                                    <div class="col-xs-3">
                                         #
                                    </div>
                                    <div class="col-xs-7">
                                         HC
                                    </div>
                                    <hr style="margin-bottom: 5px; max-width: 70%; margin-left: 15px" align="left"/>
                                </div>

                                <div class="row" v-for="t in order.Transactions">
                                    <div class="col-xs-3">
                                         @{{t.title | extract_number}}
                                    </div>
                                    <div class="col-xs-7">
                                         <span v-if="t.variations[0]">@{{t.variations[0].formatted_value | format_color}}</span>
                                         <span v-else></span> <span v-if="t.quantity > 1" style="color: #3097D1">(@{{t.quantity}})</span>
                                    </div>
                                    {{-- <h1 v-if="t.variations[1]" style="color: red;"><b>EXTRA VARIATION!!!!</b></h1> --}}
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <!-- possible buttons depending on status -->
                                <h4 style="color: #3097D1"><b>Mark As<b/></h4>
                               <button v-if="current_status == 'ready_to_cut' || current_status == 'all'" @click="markAs(index, 'cut')" style="margin-top: 5px" type="button" v-bind:class="{ disabled: (order.message_from_seller == 'cut') || (order.message_from_seller == 'packaged') || (order.message_from_seller == 'shipped') }" class="btn btn-default btn-sm orange_glow">
                                   <span class="glyphicon glyphicon-scissors" aria-hidden="true"></span> Cut
                               </button>

                               <button v-if="current_status == 'ready_to_package' || current_status == 'all'" @click="markAs(index, 'packaged')" style="margin-top: 5px" type="button" v-bind:class="{ disabled: (order.message_from_seller == 'packaged') || (order.message_from_seller == 'shipped') }" class="btn btn-default btn-sm blue_glow">
                                   <span class="glyphicon glyphicon-inbox" aria-hidden="true"></span> Packaged
                               </button>

                               <button v-if="current_status == 'ready_to_ship' || current_status == 'all'" @click="markAs(index, 'shipped')" style="margin-top: 5px" type="button" v-bind:class="{ disabled: order.message_from_seller == 'shipped' }" class="btn btn-default btn-sm green_glow">
                                   <span class="glyphicon glyphicon-envelope" aria-hidden="true"></span> Shipped
                               </button>

                               <button @click="markAs(index, 'none')" style="margin-top: 5px" type="button" class="btn btn-default btn-sm gray_glow">
                                     Clear Status
                               </button>
                               <h4 v-if="saving_status && (index_being_saved == index)" style="margin-top: 20px; color: #08c"> Saving... </h4>

                       <!-- end of possible buttons -->
                            </div>

                        </div> <!-- row -->
                        <div class="alert alert-info" role="alert" v-if="order.message_from_buyer" style="padding: 5px; margin-top:10px"><p>@{{ order.message_from_buyer }}</p></div>

                        <h3 v-if="order.note" style="color: red">Note:</h3>@{{order.note}}
                        <div class="input-group" style="margin-top: 20px">
                          <input type="text" v-model="order.note"class="form-control" placeholder="Note...">
                          <span class="input-group-btn">
                            <button class="btn btn-secondary" @click="addNote(index, order.note)" type="button">Save</button>
                          </span>
                        </div>
{{--
                        <input type="text" style="margin-top: 10px" class="form-control" v-model="order.note"><button class="btn btn-primary" @click="addNote(index, order.note)">Save</button> --}}
                        {{-- <div class="alert alert-success" role="alert" v-if="order.note" style="padding: 5px; margin-top:10px"><p>@{{ order.note }}</p></div> --}}
                        <hr style="margin-bottom: 10px; margin-top: 25px" v-bind:class="[{ orange_glow: order.message_from_seller == 'cut' }, { blue_glow: order.message_from_seller == 'packaged' }, { green_glow: order.message_from_seller == 'shipped' }]"/>

                    </div> <!-- div v-for -->

                </div> <!-- panel body -->

            </div> <!-- panel default -->

            <button class="btn btn-primary btn-md" style="text-align: center; margin-bottom: 100px" @click="loadMore" v-if="list.length < order_count">Show More</button>
            {{-- <span style="margin-left: 20px;" v-if="loading_more">Loading...</span> --}}

        </div>
    </div>

    <!-- - - - -  - - - - - - END LIST OF ORDERS - - - - - - - - - - -->

</div>
@endsection

@section('footer_scripts')
    {{-- <script src="https://code.jquery.com/jquery-2.2.4.min.js"></script> --}}
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.1/js/bootstrap-datepicker.min.js"></script>
    <script>
    $('.datepicker').datepicker({ startDate: this.startDate }).on('changeDate', function(event) {
        $('#hiddendate').val(event.date);

    });
    $('.status-button').on("click", function(){
        $('.status-button').removeClass('active');
        if (! $(this).hasClass('active')){
            $(this).addClass('active');
        }
    });
    </script>
@endsection
