@component('mail::message')

Welcome to the team! We're excited for you to help us with our business.

@component('mail::button', ['url' => 'http://ppctaxi.com'])
Get to Work
@endcomponent

@component('mail::panel', ['url' => ''])
Some inspirational quote
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
