@extends('layouts.admin')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">

            <div class="panel panel-default">
              <!-- Default panel contents -->
              <div class="panel-heading">
                  <b>Users</b>
              </div>

              <!-- Table -->
              <div class="table-responsive">
                  <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Role</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr v-for="u in users">
                            <td>@{{u.name}}</td>
                            <td>@{{u.email}}</td>
                            <td>@{{u.role}}</td>
                            <td><a href="#" @click="deleteUser">Delete</td>
                            <td><a href="#" @click="editUser">Edit</td>
                            <td v-if="u.role != 'admin'"><a href="#" @click="makeAdmin">Admin</td>
                            <td v-else-if="u.role == 'admin'" ><a href="#" @click="makeUser">User</td>
                        </tr>

                    </tbody>
                  </table>
              </div>
            </div> <!-- panel default -->
            <form class="form-inline" v-on:submit.prevent="addUser">
                  <div class="form-group">
                    <label for="exampleInputName2">Name</label>
                    <input type="text" v-model="new_user_name" class="form-control" id="name">
                  </div>
                  <div class="form-group">
                    <label for="">Email</label>
                    <input type="email" v-model="new_user_email" class="form-control" id="email">
                  </div>
                  <button type="submit" class="btn btn-default">Add User</button>
            </form>
    </div>
</div>
@endsection
