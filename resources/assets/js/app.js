
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
require('moment-timezone');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
Vue.component('example', require('./components/Example.vue'));
Vue.filter('extract_number', function (title) {
    return title.substring(0, 4);
})

Vue.filter('cleanHtml', function (str) {
    var div = document.createElement('div');
    div.appendChild(document.createTextNode(str));
    return div.innerHTML;
})

Vue.filter('format_color', function (color) {
    if (color == 'Original Red') {
        return 'Red';
    }
    else {
        return color;
    }
});

const app = new Vue({
    el: '#app',
    data: function(){
      	return {
        	startDate: moment().startOf('day').format('MM/DD/YYYY'),
            cancelDate: moment().format('MM/DD/YYYY'),
            list: [],
            order_count: 0,
            current_offset: 0,
            next_offset: 0,
            min_date: null,
            max_date: null,
            number_cut: 0,
            number_packaged: 0,
            number_shipped: 0,
            ready_to_package: 0,
            ready_to_ship: 0,
            ready_to_cut: 0,
            shipped: 0,
            saving_status: false,
            index_being_saved: 0,
            all_loaded: true,
            current_status: 'all',
            ready_to_cut_list: [],
            ready_to_package_list: [],
            ready_to_ship_list: [],
            shipped_list: [],
            loading_more: false
        }

    },
    computed: {
        // a computed getter
        activeList: function () {
          // `this` points to the vm instance
          if (this.current_status == 'all'){
              return this.list;
          }
          if (this.current_status == 'ready_to_cut'){
              return this.ready_to_cut_list;
          }
          if (this.current_status == 'ready_to_package'){
              return this.ready_to_package_list;
          }
          if (this.current_status == 'ready_to_ship'){
              return this.ready_to_ship_list;
          }
          if (this.current_status == 'shipped'){
              return this.shipped_list;
          }
        }
    },
    mounted(){
        //this.getOrdersAxios();
    },
    methods: {
        getOrders: function(date){
            $.getJSON('api/orders/' + this.date, function(results){
                this.list = results;
            });
        },
        createLists: function(){
            that.ready_to_package_list = []
            that.ready_to_ship_list = []
            that.shipped_list = []
            that.ready_to_cut_list = []
            that = this;

            that.list.forEach(function(order){
                if (order.message_from_seller == 'cut'){
                    that.ready_to_package_list.push(order);
                }
                else if (order.message_from_seller == 'packaged'){
                    that.ready_to_ship_list.push(order);
                }
                else if (order.message_from_seller == 'shipped'){
                    that.shipped_list.push(order);
                }
                else {
                    that.ready_to_cut_list.push(order);
                }
            })
        },
        shipAll: function(){
            that = this;
            that.ready_to_ship_list.forEach(function(order, index){
                that.markAs(index, 'shipped');
            })
        },
        updateLeftPanel: function(){
            //console.log('updateLeftPanel');
            vm = this;
            vm.ready_to_package = 0;
            vm.ready_to_ship = 0;
            vm.shipped = 0;
            vm.ready_to_cut = 0;

            vm.number_cut = 0;
            vm.number_packaged = 0;
            vm.number_shipped = 0;

            this.list.forEach(function(order){
                if (order.message_from_seller == 'cut'){
                    //console.log('ready to package')
                    vm.ready_to_package++;
                    vm.number_cut++;
                }
                else if (order.message_from_seller == 'packaged'){
                    //console.log('ready to ship')
                    vm.ready_to_ship++;
                    vm.number_packaged++;
                }
                else if (order.message_from_seller == 'shipped'){
                    //console.log('shipped')
                    vm.shipped++;
                    vm.number_shipped++;
                }
                else {
                    vm.ready_to_cut++;
                }
            });
            that.createLists();
            this.all_loaded = true;
            this.loading_more = false;
        },
        // getStatuses: function(offset){
        //     promises = [];
        //     that = this;
        //     console.log('incoming offset', offset);
        //     if (offset > 0){
        //         offset--;
        //     }
        //     console.log('get statuses');
        //     for (var i = offset; i < this.list.length; i++) {
        //         promises.push(axios.get('api/orders/status/'+this.list[i].order_id));
        //     }
        //     axios.all(promises).then(function(results) {
        //         console.log(results);
        //         for (var j = 0; j <= results.length - 1; j++) {
        //             that.list[j+offset].message_from_seller = results[j].data;
        //         }
        //         that.updateLeftPanel();
        //         that.createLists();
        //     });
        // },
        getDBOrders: function(offset){
            promises = [];
            that = this;
            //console.log('incoming offset', offset);
            if (offset > 0){
                offset--;
            }
            //console.log('get notes');
            for (var i = offset; i < this.list.length; i++) {
                promises.push(axios.get('api/orders/'+this.list[i].order_id));
            }
            axios.all(promises).then(function(results) {
                console.log(results);
                for (var j = 0; j <= results.length - 1; j++) {
                    that.list[j+offset].note = results[j].data.note;
                    that.list[j+offset].message_from_seller = results[j].data.status;
                }
                that.updateLeftPanel();
                that.createLists();
            });
        },
        parseResponse: function(response){
            console.log('response', response);
            this.list = response[0].results;
            this.order_count = response[0].count;
            this.next_offset = response[0].pagination.next_offset;
            this.current_offset = response[0].pagination.effective_offset;
        },
        parseMore: function(response){
            console.log('response', response);
            //this.list.push(response[0].results);
            this.list = this.list.concat(response[0].results)
            //this.order_count = response[0].count;
            this.next_offset = response[0].pagination.next_offset;
            this.current_offset = response[0].pagination.effective_offset;
        },
        getOrdersAxios: function(){
            this.number_cut = 0;
            this.number_packaged = 0;
            this.number_shipped = 0;
            this.all_loaded = false;
            this.list = [];
            var date = $('#hiddendate').val();
            this.startDate = moment(date).startOf('day').format('MM/DD/YYYY');
            this.min_date = moment(date).tz('America/New_York').startOf('day').format('X');
            this.max_date = moment(date).tz('America/New_York').endOf('day').format('X');

            that = this;
            promises = [axios.get('api/orders/'+this.min_date+'/'+this.max_date+'/offset/'+this.current_offset).then(response => this.parseResponse(response.data))];

            axios.all(promises).then(function() {

                that.getDBOrders(0)
                //that.getStatuses(0)


            });
        },
        loadMore: function(){
            if (this.list == this.order_count){
                return;
            }
            this.loading_more = true;
            that = this;
            promises = [axios.get('api/orders/'+this.min_date+'/'+this.max_date+'/offset/'+this.next_offset).then(response => this.parseMore(response.data))];

            that = this;
            axios.all(promises).then(function() {
                that.getDBOrders(that.current_offset);
            });
        },
        addNote: function(order_index, note){
            this.saving_status = true;
            this.index_being_saved = order_index;
            var matching_order = this.activeList[order_index]
            this.activeList[order_index].note = note;

            axios.post('api/orders/update', {
                etsy_order_id: matching_order.order_id,
                note: note,
                creation_tsz: matching_order.creation_tsz
              })
              .then(function (response) {
                console.log(response);
                // update left panel
                that.createLists();
                that.updateLeftPanel();
                that.saving_status = false;
              })
              .catch(function (error) {
                console.log(error);
              });
        },
        markAs: function(order_index, status){
            this.saving_status = true;
            this.index_being_saved = order_index;
            var matching_order = this.activeList[order_index]
            this.activeList[order_index].message_from_seller = status;
            that = this;
            axios.post('api/orders', {
                etsy_order_id: matching_order.order_id,
                status: status,
                creation_tsz: matching_order.creation_tsz
              })
              .then(function (response) {
                console.log(response);
                // update left panel
                that.createLists();
                that.updateLeftPanel();
                that.saving_status = false;
              })
              .catch(function (error) {
                console.log(error);
              });
        }
    }
});
