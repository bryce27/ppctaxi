
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
require('moment-timezone');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */


const app = new Vue({
    el: '#app',
    data: function(){
      	return {
        	users: [],
            new_user_name: '',
            new_user_email: '',
        }
    },
    mounted(){
        this.getUsers();
    },
    methods: {
        getUsers: function(){
            axios.get('api/users').then(results => this.users = results.data);
        },
        addUser: function(){
            that = this;
            alert('addUser');
            axios.post('api/users', {
                name: this.new_user_name,
                email: this.new_user_email,
              })
              .then(function (response) {
                console.log(response.data[0]);
                // update left panel
                that.users.push(response.data[0]);
                alert('Success');
              })
              .catch(function (error) {
                console.log(error);
                alert('Failure');
              });
        }
    }
});
